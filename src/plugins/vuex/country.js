import axios from "./axios";

export default {
    actions: {
        fetchCountries(context) {
            return new Promise((resolve, reject) => {
                axios
                    .get('/countries')
                    .then((response) => {
                        console.log("Countries olindi")
                        console.log(response)

                        let countries = {
                            models: response.data['hydra:member'],
                            totalItems: response.data['hydra:totalItems']
                        }

                        context.commit('updateCountries', countries)
                        resolve()
                    })
                    .catch(() => {
                        console.log('Countriesni olishda xatolik yuz berdi')
                        reject()
                    })
            })
        },
    },
    mutations: {
        updateCountries(state, countries) {
            state.countries = countries
        },
    },

    state: {
        countries: {
            models: [],
            totalItems: 0
        }
    },

    getters: {
        getCountries(state) {
            return state.countries.models
        },
    }
}