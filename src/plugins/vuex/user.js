import axios from "./axios";

export default {
    actions: {
        fetchToken(context, data) {
            return new Promise((resolve, reject) => {
                axios
                    .post('/users/auth', data)
                    .then((response) => {
                        console.log('Token olindi')
                        console.log(response)

                        context.commit('updateToken', response.data)
                        resolve()
                    })
                    .catch(() => {
                        console.log('Token olishda xatolik yuz berdi')
                        reject()
                    })
            })
        },
        refreshToken(context, data) {
            return new Promise((resolve, reject) => {
                axios
                    .post('/users/auth/refreshToken', data)
                    .then((response) => {
                        console.log('Token yangilandi')
                        console.log(response)

                        context.commit('updateToken', response.data)
                        resolve()
                    })
                    .catch(() => {
                        console.log('Token yangilashda xatolik yuz berdi')
                        reject()
                    })
            })
        },
        fetchUser(context, userId) {
            return new Promise((resolve, reject) => {
                axios
                    .get('/users/' + userId)
                    .then((response) => {
                        console.log('User olindi')
                        console.log(response)

                        context.commit('updateUser', response.data.user)
                        resolve()
                    })
                    .catch(() => {
                        console.log('User qushishda xatolik yuz berdi')
                        reject()
                    })
            })
        },
        fetchAboutMe(context) {
            return new Promise((resolve, reject) => {
                axios
                    .get('/users/about_me')
                    .then((response) => {
                        console.log('about me  olindi')
                        console.log(response)

                        context.commit('updateAboutMe', response.data.user)
                        resolve()
                    })
                    .catch(() => {
                        context.commit('clearTokens')
                        console.log('User qushishda xatolik yuz berdi')
                        reject()
                    })
            })
        },
        pushUser(context, data) {
            return new Promise((resolve, reject) => {
                axios
                    .post('/users', data)
                    .then((response) => {
                        console.log("User qo'shildi")
                        console.log(response)

                        let user = {
                            "@id": response.data['@id'],
                            id: response.data.id,
                            email: response.data.email,
                            roles: response.data.roles,
                            createdAt: response.data.createdAt,
                            updatedAt: response.data.updatedAt,
                            persons: response.data.persons
                        }


                        context.commit('updateUser', user)
                        resolve()
                    })
                    .catch(() => {
                        console.log('User qushishda xatolik yuz berdi')
                        reject()
                    })
            })
        },
    },
    mutations: {
        updateToken(state, tokens) {
            localStorage.setItem('accessToken', tokens.accessToken)
            localStorage.setItem('refreshToken', tokens.refreshToken)

            state.accessToken = tokens.accessToken
            state.refreshToken = tokens.refreshToken
        },
        updateUser(state, user) {
            state.user = user
        },
        updateAboutMe(state, user) {
            state.aboutMe = user
        },
        clearTokens(state) {
            state.accessToken = null
            state.refreshToken = null

            localStorage.setItem('accessToken', null)
            localStorage.setItem('refreshToken', null)
        }
    },

    state: {
        accessToken: localStorage.getItem("accessToken"),
        refreshToken: localStorage.getItem("refreshToken"),

        user: {
            id: null,
            email: null,
            roles: null,
            createdAt: null,
            updatedAt: null,
            persons: null
        },
        aboutMe: {
            id: null,
            email: null,
            roles: null,
            createdAt: null,
            updatedAt: null,
            persons: null
        }
    },

    getters: {
        getAccessToken(state) {
            return state.accessToken
        },
        getRefreshToken(state) {
            return state.refreshToken
        },

        getUser(state) {
            return state.user
        },
        isAuthorized(state) {
            return state.accessToken !== null
        }
    }
}