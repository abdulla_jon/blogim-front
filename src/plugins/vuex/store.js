import {createStore} from "vuex"
import user from "@/plugins/vuex/user"
import country from "@/plugins/vuex/country";

export default createStore({
    modules: {
        country,
        user
    }
})