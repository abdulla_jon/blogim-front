import {createRouter, createWebHistory} from "vue-router"
import HomePage from "@/pages/HomePage"
import store from "@/plugins/vuex/store"

const ifNotAuthorized = (to, from, next) => {
    if (store.getters.isAuthorized) {
        next('/')
    } else {
        next()
    }
}
const ifAuthorized = (to, from, next) => {
    if (store.getters.isAuthorized) {
        next()
    } else {
        next('/login')
    }
}
const routes = [
    {
        path: '',
        component: HomePage,
        beforeEnter: ifAuthorized
    },
    {
        path: '/login',
        component: () => import('@/pages/LoginPage'),
        beforeEnter: ifNotAuthorized
    },
    {
        path: '/sign-up',
        component: () => import('@/pages/SignUpPage'),
        beforeEnter: ifNotAuthorized
    },
]
export default createRouter({
    history: createWebHistory(),
    routes
})